function get_books(find) {
    $.ajax({
        url : "https://www.googleapis.com/books/v1/volumes?q=" + find,
        type : 'GET',
        success : function(result) {
            var item = result.items;
            var arrays =[];
            var counter = 0;
            for (i in item) { 
                var b = item[i];
                arrays.push(b)
                counter++;
                if (counter == 10) {
                    break;
                }
            }

            $("tbody").empty();
            
            for (i in arrays) {
                $.ajax({
                    url : "/likes/" + JSON.stringify(arrays[i].id),
                    type : "GET",
                    async : false,
                    success : function(res) {
                        console.log()
                        var image = arrays[i].volumeInfo.imageLinks.smallThumbnail == undefined ? "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Question_mark_%28black%29.svg/174px-Question_mark_%28black%29.svg.png" : arrays[i].volumeInfo.imageLinks.smallThumbnail
                        var holder = "<tr><td> <img src='" + arrays[i].volumeInfo.imageLinks.smallThumbnail  
                        + "'width='100px' height='100px'></td>"+ "<td>" +  arrays[i].volumeInfo.title 
                        + "</td>" + "<td>" +  arrays[i].volumeInfo.authors + "</td>" + "<td>" 
                        +  arrays[i].volumeInfo.publishedDate + "</td>"+"<td>"+"<p class='identitas'>" + res.likes 
                        + "</p>" + "<button id = " + arrays[i].id + " class='like like" + i +" btn btn-dark' titles=" + JSON.stringify(arrays[i].volumeInfo.title) + "> Like </button>" + "</td>" + "</tr>";
                        $("tbody").append(holder);
                        holder = " ";
                    }
                })                             
            }
        }
    }) 
}

$('#find').click(function(){
    get_books($('input[name=search]').val())
})

$("tbody").click((e) => {
    if ($(e.target).hasClass("like")) {
        var ids = $(e.target).attr("id")
        var nama = $(e.target).attr("titles")
        $.post($("meta[name='untukurl']").attr("content"), {
            nama : nama,
            id_buku : ids,
        }).done(() => {
            $.get("/likes/" + ids, (res) => {
                console.log("lythfi")
                $(e.target).prev().text(res.likes)
            })
        })
  
    }
})

$('.modal-like').click(function(){
    $(".modal-body").empty()
    $.ajax({
        url : '/top5',
        type : 'GET',
        success : function(res) {
            var holds = res.semua_buku
            for (i in holds) {
                var temp = "<div class='demi_modal d-flex flex justify-content-between'> <h3>" + holds[i].nama + "</h3>" 
                + "<h3> Likes : " + holds[i].likes + "</h3></div>"
                $(".modal-body").append(temp);
                temp = " "
            }
        }
    })

})
