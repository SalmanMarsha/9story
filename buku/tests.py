from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .models import Library
from .apps import BukuConfig
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class AppsConf(TestCase):
    def test_app(self):
        self.assertEqual(BukuConfig.name, 'buku')
        self.assertEqual(apps.get_app_config('buku').name, 'buku')

class UrlRouting(TestCase):
    def test_url1_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

class ModelUnit(TestCase):
    def test_buku_models(self):
        Library.objects.create(
            id_buku='q1LVDAAAQBAJ',
            nama='Haha And Hehe Have Fun : All Set To Read')
        count = Library.objects.all().count()
        self.assertEqual(count, 1)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(FuncTest, self).tearDown()
    
    def test_search(self):
        self.browser.get(self.live_server_url)
        search_box = self.browser.find_element_by_name('search')
        button = self.browser.find_element_by_id('find')

        #ngambil isi tag tbody sebelum diklik
        tbody_awal = self.browser.find_element_by_tag_name('tbody').text
        time.sleep(4)
        search_box.send_keys("harry")
        time.sleep(4)
        button.click()
        time.sleep(4)
        #ngambil isi tag tbody setelah diklik
        tbody_akhir = self.browser.find_element_by_tag_name('tbody').text
        #membadingkan tag tbody awal dan akhir berbeda setelah diklik
        self.assertNotEqual(tbody_awal,tbody_akhir)
    
    def test_likes_and_modal(self):
        self.browser.get(self.live_server_url)
        search_box = self.browser.find_element_by_name('search')
        button = self.browser.find_element_by_id('find')
        time.sleep(8)
        search_box.send_keys("cari")
        time.sleep(8)
        button.click()
        time.sleep(8)

        #menyimpan nilai awal likes sebelum diklik
        likes_awal = self.browser.find_element_by_class_name('identitas').text
        button_like = self.browser.find_element_by_class_name('like')
        button_like.click()
        time.sleep(8)

        #menyimpan nilai akhir likes setelah diklik
        likes_akhir = self.browser.find_element_by_class_name('identitas').text

        #membandingkan bahwa likes awal dan akhir berbeda setelah diklik
        self.assertNotEqual(likes_awal,likes_akhir)

        #klik like elemen ke 2 agar terdaftar di modal
        button_like2 = self.browser.find_element_by_class_name('like1')
        button_like2.click()
        time.sleep(3)

        #klik like elemen ke 3 agar terdaftar di modal
        button_like3 = self.browser.find_element_by_class_name('like2')
        button_like3.click()
        time.sleep(3)

        #klik like elemen ke 4 agar terdaftar di modal
        button_like4 = self.browser.find_element_by_class_name('like3')
        button_like4.click()
        time.sleep(3)
        #klik like elemen ke 5 agar terdaftar di modal
        button_like5 = self.browser.find_element_by_class_name('like4')
        button_like5.click()
        time.sleep(3)
        #klik like elemen ke 6 agar terdaftar di modal
        button_like6 = self.browser.find_element_by_class_name('like5')
        button_like6.click()
        time.sleep(3)
        
        modal_butt = self.browser.find_element_by_class_name('modal-like')
        time.sleep(8)
        modal_butt.click()
        time.sleep(8)

        #ngecek apakah likes yang digenerate di js ada tidak di halaman
        self.assertIn('Top 5', self.browser.page_source)

        #ngecek apakah likes yang digenerate di js ada tidak di halaman
        
        self.assertIn('Likes', self.browser.page_source)