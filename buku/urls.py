from django.urls import path
from . import views

app_name="buku"

urlpatterns = [
    path('',views.index,name="index"),
    path('add/', views.add, name='add'),
    path('likes/<str:id>', views.likes, name="likes"),
    path('top5/', views.top5, name="top5")
]

