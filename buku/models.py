from django.db import models

# Create your models here.
class Library(models.Model):
    nama = models.CharField('nama',max_length=3000)
    id_buku = models.CharField('id_buku',max_length=3000, default="AAAAAAAAAAAA")
    likes = models.PositiveIntegerField(default=0)
