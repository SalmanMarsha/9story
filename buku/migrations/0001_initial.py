# Generated by Django 3.0.5 on 2020-05-01 03:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Library',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=3000, verbose_name='nama')),
                ('id_buku', models.CharField(default='AAAAAAAAAAAA', max_length=3000, verbose_name='id_buku')),
                ('likes', models.PositiveIntegerField(default=0)),
            ],
        ),
    ]
