from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Library
import json


# Create your views here.
def index(request):
    return render(request,'index.html')

@csrf_exempt
def add(request):
    if request.method == "POST":
        contain = request.POST["id_buku"]
        obj = Library.objects.all().filter(id_buku__icontains= contain).count()
        if obj == 0:
            Library.objects.create(nama = request.POST["nama"], id_buku = request.POST["id_buku"])
    return redirect("buku:index")

@csrf_exempt
def likes(request,id):
    try:
        book_like = Library.objects.get(id_buku=id)
        book_like.likes +=1
        book_like.save()
        return JsonResponse({'likes':book_like.likes})
    except:
        return JsonResponse({'likes':0})

def top5(request):
    return JsonResponse({'semua_buku':list(Library.objects.all().order_by('-likes')[:5].values())})