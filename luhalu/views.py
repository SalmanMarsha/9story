from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def index(request):
    return render(request,'indexs.html')

def register(request):
    form = UserCreationForm()
    context = {
        'form': form,
    }
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('luhalu:user_login')
        else :
            context.update({'pesan':'invalid request, terdapat kesalahan'})      
    return render(request,'register.html',context=context)


def user_login(request):
    if request.method == "POST":
        userr = request.POST['username']
        passw = request.POST['password']
        user = authenticate(request, username=userr, password=passw)
        if user != None:
            login(request, user)
            return redirect('luhalu:index')
        else:
            context = {
                'pesan':'invalid request, akun tidak terdaftar'
            }
            return render(request, 'login.html', context=context)
    return render(request, 'login.html')

def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('luhalu:index')