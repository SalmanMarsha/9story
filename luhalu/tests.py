from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from django.contrib.auth.forms import UserCreationForm
from .apps import LuhaluConfig
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class AppsConf(TestCase):
    def test_app(self):
        self.assertEqual(LuhaluConfig.name, 'luhalu')
        self.assertEqual(apps.get_app_config('luhalu').name, 'luhalu')

class UrlRouting(TestCase):
    def test_url1_is_exist(self):
        response = Client().get('/luhalu/')
        self.assertEqual(response.status_code, 200)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(FuncTest, self).tearDown()
    
    def test_keseluruhan_gan(self):
        self.browser.get(self.live_server_url+ '/luhalu')
        time.sleep(5)
        daftarbut = self.browser.find_element_by_class_name('reg-but')
        daftarbut.click()
        time.sleep(5)
        req_name = self.browser.find_element_by_name('username')
        req_pass1 = self.browser.find_element_by_name('password1')
        req_pass2 = self.browser.find_element_by_name('password2')
        sub_but = self.browser.find_element_by_class_name('reg-bro')
        time.sleep(5)
        req_name.send_keys("salmanganteng1")
        time.sleep(5)
        req_pass1.send_keys("haikamu21")
        time.sleep(5)
        req_pass2.send_keys("haikamu21")
        time.sleep(5)
        sub_but.click()
        time.sleep(5)

        login_user = self.browser.find_element_by_id('username')
        login_pass = self.browser.find_element_by_id('password')
        login_sub = self.browser.find_element_by_class_name('log-buty')
        time.sleep(5)
        login_user.send_keys("salmanganteng1")
        time.sleep(5)
        login_pass.send_keys("haikamu21")
        time.sleep(5)
        login_sub.click()
        time.sleep(5)

        self.assertIn("salmanganteng1",self.browser.page_source)

        logout_but = self.browser.find_element_by_id('logout-but')
        logout_but.click()

        self.assertIn("Hai ganteng",self.browser.page_source)

    def test_register_tidak_valid_gan(self):
        self.browser.get(self.live_server_url+ '/luhalu')
        time.sleep(5)
        daftarbut = self.browser.find_element_by_class_name('reg-but')
        daftarbut.click()
        time.sleep(5)
        req_name = self.browser.find_element_by_name('username')
        req_pass1 = self.browser.find_element_by_name('password1')
        req_pass2 = self.browser.find_element_by_name('password2')
        sub_but = self.browser.find_element_by_class_name('reg-bro')
        time.sleep(5)
        req_name.send_keys("salmanganteng1")
        time.sleep(5)
        req_pass1.send_keys("haikamu2")
        time.sleep(5)
        req_pass2.send_keys("haikamu21")
        time.sleep(5)
        sub_but.click()
        time.sleep(5)
        self.assertIn('invalid request, terdapat kesalahan',self.browser.page_source)
    
    def test_login_tidak_valid_gan(self):
        self.browser.get(self.live_server_url+ '/luhalu')
        time.sleep(5)
        daftarbut = self.browser.find_element_by_class_name('login-but')
        time.sleep(5)
        daftarbut.click()

        login_user = self.browser.find_element_by_id('username')
        login_pass = self.browser.find_element_by_id('password')
        login_sub = self.browser.find_element_by_class_name('log-buty')
        time.sleep(5)
        login_user.send_keys("salmanganteng1")
        time.sleep(5)
        login_pass.send_keys("haikamu21")
        time.sleep(5)
        login_sub.click()
        time.sleep(5)

        self.assertIn('invalid request, akun tidak terdaftar',self.browser.page_source)